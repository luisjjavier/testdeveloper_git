﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PruebaJson
{
    public class Contactos
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public UserType Type { get; set; }

        public int Edad()
        {
            return Age;
        }
        public int TipoDeUsuario()
        {
            return (int)this.Type;
        }


    }
    public enum UserType
    {
        Employee = 1,
        Administrator = 2

    }

}
