﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PruebaJson
{
    public class XmlInvoker
    {
        public List<string> GetResponse(string metodo, string rutaBase, string campo, Dictionary<string, string> parametros)
        {
            WebRequest webRequest = WebRequest.Create(rutaBase + metodo);
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "application/json; charset=utf-8";
            httpRequest.Accept = "application/xml";

            Stream requestStream = httpRequest.GetRequestStream();
            using (StreamWriter writer = new StreamWriter(requestStream))
            {
                writer.Write(ToJson(parametros));
                writer.Flush();
            }
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException webEx)
            {
                response = (HttpWebResponse)webEx.Response;
            }
            XmlDocument document = new XmlDocument();

            string body = string.Empty;

            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {

                body = reader.ReadToEnd();
                response.Close();
            }
            List<string> cuentas = new List<string>();
            if (!string.IsNullOrEmpty(body))
            {


                document.LoadXml(body);

                foreach (XmlNode i in document.DocumentElement.ChildNodes)
                {
                    var obj = FromJSON<JObject>(JsonConvert.SerializeXmlNode(i.ChildNodes[0]));
                    cuentas.Add(obj[campo].ToString());
                    Console.WriteLine(obj[campo].ToString());
                }

            }

            return new List<string>(); ;
           

        }
        public string ToJson(object obj)
        {
            JsonSerializer serializer = new JsonSerializer();
            StringBuilder sb = new StringBuilder();
            using (StringWriter textWriter = new StringWriter(sb))
            {
                JsonTextWriter writer = new JsonTextWriter(textWriter);
                serializer.Serialize(writer, obj);
                writer.Flush();
            }

            return sb.ToString();
        }

        public T FromJSON<T>(string data)
        {
            JsonSerializer serializer = new JsonSerializer();


            T obj;
            using (StringReader textReader = new System.IO.StringReader(data))
            {
                JsonTextReader reader = new JsonTextReader(textReader);
                obj = (T)serializer.Deserialize(reader, typeof(T));
            }

            return obj;
        }
    }
}
